<?php

use App\User;
use App\Mail\Reciever;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('/prereg', 'Auth\RegisterController@preRegister')->name('prereg');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/contact', 'SiteController@contact')->name('contact');
Route::view('/company', 'company')->name('company');
Route::view('/faq', 'faq')->name('faq');
Route::view('/terms', 'terms')->name('terms');
Route::view('/pricing', 'pricing')->name('pricing');
Route::view('/sitemap', 'sitemap')->name('sitemap');
Route::get('/checkout', 'SiteController@checkout')->name('checkout');
Route::post('/mailing', 'SiteController@mailing')->name('mailing');

Route::get('/user/{id}', 'ReceiversController@getReciever')->name('reciever.get');


Route::middleware('auth')->group(function () {

    Route::get('/users', 'SiteController@getUsers');

//account
    Route::get('/account', 'AccountController@index')->name('account');
    Route::post('/basic/change', 'AccountController@basic')->name('basic.change');
    Route::post('/password/change', 'AccountController@password')->name('password.change');
    Route::post('/plan/change', 'AccountController@plan')->name('plan.change');
    Route::post('/billing/change', 'AccountController@billing')->name('billing.change');
    Route::post('/card/change', 'AccountController@card')->name('card.change');
    Route::post('/will/change', 'AccountController@will')->name('will.change');
    Route::post('/delete/user', 'AccountController@deleteUser');


//recievers
    Route::get('/recievers', 'ReceiversController@index')->name('recievers');
    Route::post('/recievers/{reciever}/cover', 'ReceiversController@cover');
    Route::post('/recievers/{reciever}/avatar', 'ReceiversController@avatar');
    Route::post('/recievers/{reciever}/name', 'ReceiversController@name');
    Route::post('/recievers/{reciever}/thumb', 'ReceiversController@thumbUpload');
    Route::post('/recievers/{reciever}/details', 'ReceiversController@details');
    Route::get('/recievers/{reciever}/download', 'ReceiversController@videoDownload');
    Route::match(['post', 'get'], '/recievers/video/upload', 'ReceiversController@videoUpload');


//profile
    Route::get('/profile/{nick?}', 'ProfileController@index')->name('profile');
    Route::post('/profile/change/cover', 'ProfileController@uploadCover');
    Route::post('/profile/change/avatar', 'ProfileController@uploadAvatar');
    Route::post('/profile/change/wisdom', 'ProfileController@wisdom')->name('profile.wisdom');

//events
    Route::post('/event/validate', 'EventsController@validateData');
    Route::post('/event/save', 'EventsController@save');
    Route::match(['post', 'get'], '/event', 'EventsController@store')->name('event');
    Route::post('/event/delete', 'EventsController@destroy')->name('event.delete');

//trustee
    Route::post('/trustee', 'TrusteeController@store')->name('trustee');
    Route::post('/trustee/release', 'TrusteeController@release')->name('trustee.release');


});






