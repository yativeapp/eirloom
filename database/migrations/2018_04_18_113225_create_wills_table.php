<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wills', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->longText('facebook')->nullable();
            $table->longText('youtube')->nullable();
            $table->longText('googleplus')->nullable();
            $table->longText('linkedin')->nullable();
            $table->longText('instagram')->nullable();
            $table->longText('twitter')->nullable();
            $table->longText('snapchat')->nullable();
            $table->longText('pinterest')->nullable();
            $table->longText('blogger')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wills');
    }
}
