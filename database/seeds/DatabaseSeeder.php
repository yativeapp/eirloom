<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlansSeeder::class);

        factory('App\User', 1)->create([
            'firstname' => 'waleed',
            'email' => 'wkasem2@yahoo.com',
            'password' => \Hash::make('1234')
        ]);
    }
}
