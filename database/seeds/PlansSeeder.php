<?php

use Illuminate\Database\Seeder;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $plans = [
            ['name' => 'basic', 'yearly' => 5000, 'monthly' => 499, 'count' => 3],
            ['name' => 'premuim', 'yearly' => 7000, 'monthly' => 699, 'count' => 6],
            ['name' => 'ultimate', 'yearly' => 9900, 'monthly' => 999, 'count' => 9]
        ];


        App\Plans::insert($plans);
    }
}
