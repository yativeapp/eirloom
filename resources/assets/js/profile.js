

try {
     window.$ = window.jQuery = require('jquery');

} catch (e) { }



import Vue from 'vue';
import Croppa from 'vue-croppa';
require('./fancy');

Vue.use(Croppa);

import LaravelVueValidator from 'laravel-vue-validator'

Vue.use(LaravelVueValidator)




Vue.component('profile-header', require('./components/ProfileHeader.vue'));
Vue.component('addevent', require('./components/AddEvent.vue'));
Vue.component('errors', require('./components/Errors.vue'));


new Vue({
     el: '#app'
});