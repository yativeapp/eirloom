


try {
    window.$ = window.jQuery = require('jquery');

} catch (e) { }


import inView from 'in-view';

require('sticky-sidebar');

window.inView = inView();


if ($('.video-section').length) {
    
    inView('.video-section')
        .on('enter', () => {
            $('.video-section .block').each((i, el) => {
 
                setTimeout(() => {
                    $(el).css('opacity', 1);
                }, i * 1000);
            });
        })
        .on('exit', el => {
            $('.video-section .block').css('opacity', 0);
        });
}


if($('.termslist-m').length){
new StickySidebar('.termslist-m', {
    topSpacing: 20,
    bottomSpacing: 20,
    containerSelector: '.hero-body',
    innerWrapperSelector: '.sidebar__inner'
});
}

$('.navbar-burger').click(() => {
$('#navbarMenuHeroC').toggle();
});


$(document).ready(function () {
    $('.subscribtion-list li').click(function () {

        $(this).siblings().find('a').removeClass('is-active');

        $(this).find('a').addClass('is-active');

        $("input[name='plan']").val($(this).find('a').data('plan'));
    });

    $('.notification .delete').click(() => {

        $('.notification').remove();
    });
});
