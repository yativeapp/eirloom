export default {
     data() {
          return {
               cover: null,
               avatar: null,
               coverLoading: false,
               avatarLoading: false
          };
     },
     computed: {
          initialCover() {
               return window.location.origin + this.profile.cover;
          },
          initialAvatar() {
               return window.location.origin + this.profile.avatar;
          },
          showSaveCover() {
               return this.cover ? this.cover.hasImage() : false;
          },
          showSaveAvatar() {
               return this.avatar ? this.avatar.hasImage() : false;
          }
     },
     methods: {
          removeIntialAvatar() {
               this.profile.avatar = null;
          },
          removeIntialCover() {
               this.profile.cover = null;
          },
          uploadCover() {
               this.coverLoading = true;
               this.cover.generateBlob(
                    blob => {
                         this.upload(blob, "cover").then(() => {
                              swal("Changed!", "Cover Changed Successfully!", "success");
                              this.profile.cover = true;
                              this.coverLoading = false;
                         });
                    },
                    "image/jpeg",
                    0.8
               );
          },
          uploadAvatar() {
               this.avatarLoading = true;
               this.avatar.generateBlob(
                    blob => {
                         this.upload(blob, "avatar").then(() => {
                              swal("Changed!", "Avatar Changed Successfully!", "success");
                              this.profile.avatar = true;
                              this.avatarLoading = false;

                         });
                    },
                    "image/jpeg",
                    0.8
               );
          },

          upload(blob, url) {
               return new Promise((resolve, reject) => {
                    var fd = new FormData();
                    fd.append("image", blob);
                    $.ajax({
                         type: "POST",
                         url: this.getUrl() + url,
                         data: fd,
                         processData: false,
                         contentType: false,
                         headers: {
                              "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                         }
                    }).done(() => {
                         resolve();
                    });
               });
          }
     }
}