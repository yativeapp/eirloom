@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else

{{ $data['firstname'] }} {{ $data['lastname'] }} Have Sent You A message

# Email : {{ $data['email'] }} --- # Number {{ $data['phonenumber'] }}
@endif

{{-- Intro Lines --}}
{{ $data['msg'] }}




{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
Regards,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
If you’re having trouble clicking the "View Profile" button, copy and paste the URL below
into your web browser: [route('reciever.get' , ['id' => $user->id]](route('reciever.get' , ['id' => $user->id])
@endcomponent
@endisset
@endcomponent
