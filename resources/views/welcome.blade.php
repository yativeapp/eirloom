@extends('layouts.app' , ['full' => true , 'white' => true]) @section('content')

<section>

     <!-- Start EasyHtml5Video.com BODY section -->
     <style type="text/css">
          .easyhtml5video .eh5v_script {
               display: none
          }

          .mobile-video{
            position:absolute;
    height: 390px;
    width: 440px;
    background:url('/imgs/phone.png') no-repeat;
          }

  

          .video-container{
            position:relative;
          }

                  .video-container video{

        width: 390px;
    padding-right: 12px;
    margin-right: 95px;
    padding:30px;

            
          }

          .block{
            opacity:0;
            height:100px;
            transition:opacity 1s ease-in-out;
          }

          .inline{
            display:inline-block;
          }

          .introVideoSection{
                position: relative;
    max-width: 1920px;
    margin: 0 auto;
    margin-top: 10px;
          }

  

          @media screen and (min-width: 769px), print {
.video-container {
    margin-left: 29.333333%;
}


}

          @media screen and (max-width: 350px), print {

.introVideoSection{
width: 600px;

margin-left: -42%;
}
}


     </style>
     <div class="easyhtml5video introVideoSection">
          <video autoplay="autoplay" poster="video/Digital_Legacy.jpg" style="width:100%" title="Digital Legacy HD" loop="loop" muted="muted"
               onended="var v=this;setTimeout(function(){v.play()},300)">
               <source src="video/Digital_Legacy.m4v" type="video/mp4" />
               <source src="video/Digital_Legacy.webm" type="video/webm" />
               <object type="application/x-shockwave-flash" data="video/flashfox.swf" width="1920" height="1080" style="position:relative;">
                    <param name="movie" value="video/flashfox.swf" />
                    <param name="allowFullScreen" value="true" />
                    <param name="flashVars" value="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/Digital_Legacy.jpg&amp;src=Digital_Legacy.m4v"
                    />
                    <embed src="video/flashfox.swf" width="1920" height="1080" style="position:relative;" flashVars="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/Digital_Legacy.jpg&amp;src=Digital_Legacy.m4v"
                         allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en"
                    />
                    <img alt="Digital Legacy HD" src="video/Digital_Legacy.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser"
                    />
               </object>
          </video>
          <div class="eh5v_script">
               <a href="http://easyhtml5video.com">html5 video converter</a> by EasyHtml5Video.com v3.9.1</div>
     </div>
     <script src="video/html5ext.js" type="text/javascript"></script>
     <!-- End EasyHtml5Video.com BODY section -->

</section>


<div class="container">

     <section class="section has-text-centered box is-shadowless video-section " style="    background-color: #a2dbdb;">
        <div class="columns">
           <div class="column block">
                         <figure class="image inline is-64x64">
                                        <img src="{{ asset('imgs/video-camera-icon.png') }}" alt="Image inline">
                                   </figure> 
                              <p class="title is-4">Shoot Your Video</p>
                         
           </div>
           <div class="column block">
                     <figure class="image inline is-64x64">
                                        <img src="{{ asset('imgs/user-icon.png') }}" alt="Image inline">
                                   </figure> 
                              <p class="title is-4">Fill Out Reciever Information</p>
           </div>
        </div>

<div class="columns">

<div class="column is-half video-container" >

           <video  autoplay="autoplay" class="mobile-video-video" title="Phone video" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
<source src="video/Phone_video_3.mp4" type="video/mp4" />
<source src="video/Phone_video.webm" type="video/webm" />
<object type="application/x-shockwave-flash" data="video/flashfox.swf" width="1280" height="720" style="position:relative;">
<param name="movie" value="video/flashfox.swf" />
<param name="allowFullScreen" value="true" />
<param name="flashVars" value="autoplay=true&amp;controls=false&amp;fullScreenEnabled=false&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/Phone_video.jpg&amp;src=Phone_video.m4v" />
 <embed src="video/flashfox.swf" width="1280" height="720" style="position:relative;"  flashVars="autoplay=true&amp;controls=false&amp;fullScreenEnabled=false&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/Phone_video.jpg&amp;src=Phone_video.m4v"	allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en" />
</object>
</video>
</div>
</div>


        <div class="columns">
           <div class="column block">
                     <figure class="image inline is-64x64">
                                        <img src="{{ asset('imgs/arrow-upload-icon.png') }}" alt="Image inline">
                                   </figure> 
                              <p class="title is-4">Upload</p>
           </div>
           <div class="column block">
                     <figure class="image inline is-64x64">
                                        <img src="{{ asset('imgs/share-icon.png') }}" alt="Image inline">
                                   </figure> 
                              <p class="title is-4">We Pass It On</p>
           </div>
        </div>
     </section>
</div>

<div class="container">

     <section class="section box is-shadowless">
          <div class="columns">
               <div class="column">
                    <h3 class="title is-3">What Else Can You Do ?</h3>
                    <div class="box is-shadowless">
                         <article class="media">
                              <div class="media-left">
                                   <figure class="image is-64x64">
                                        <img src="{{ asset('imgs/timeline-icon.png') }}" alt="Image">
                                   </figure>
                              </div>
                              <p class="title is-4">Create Timeline</p>
                         </article>
                    </div>
                    <div class="box is-shadowless">
                         <article class="media">
                              <div class="media-left">
                                   <figure class="image is-64x64">
                                        <img src="{{ asset('imgs/maps-icon.png') }}" alt="Image">
                                   </figure>
                              </div>
                              <p class="title is-4">Create a Map</p>
                         </article>
                    </div>
                    <div class="box is-shadowless">
                         <article class="media">
                              <div class="media-left">
                                   <figure class="image is-64x64">
                                        <img src="{{ asset('imgs/quote-icon.png') }}" alt="Image">
                                   </figure>
                              </div>
                              <p class="title is-4">Create a Words Of Wisdom</p>
                         </article>
                    </div>
                    <div class="box is-shadowless">
                         <article class="media">
                              <div class="media-left">
                                   <figure class="image is-64x64">
                                        <img src="{{ asset('imgs/social-group.png') }}" alt="Image">
                                   </figure>
                              </div>
                              <p class="title is-4">Create a Social Media Will</p>
                         </article>
                    </div>
               </div>
               <div class="column">
                    <!-- Start EasyHtml5Video.com BODY section -->
                    <style type="text/css">
                         .easyhtml5video .eh5v_script {
                              display: none
                         }

                    </style>
                    <div class="easyhtml5video" style="position:relative;max-width:1080px;">
                         <video autoplay="autoplay" poster="video/life_event.jpg" style="width:100%" title="life event" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
                              <source src="video/life_event.m4v" type="video/mp4" />
                              <source src="video/life_event.webm" type="video/webm" />
                              <object type="application/x-shockwave-flash" data="video/flashfox.swf" width="1080" height="1080" style="position:relative;">
                                   <param name="movie" value="video/flashfox.swf" />
                                   <param name="allowFullScreen" value="true" />
                                   <param name="flashVars" value="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/life_event.jpg&amp;src=life_event.m4v"
                                   />
                                   <embed src="video/flashfox.swf" width="1080" height="1080" style="position:relative;" flashVars="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/life_event.jpg&amp;src=life_event.m4v"
                                        allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en"
                                   />
                                   <img alt="life event" src="video/life_event.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser"
                                   />
                              </object>
                         </video>

                         <!-- End EasyHtml5Video.com BODY section -->
                    </div>
               </div>
     </section>

     <section class="section box is-shadowless">
          <!-- Start EasyHtml5Video.com BODY section -->
          <style type="text/css">
               .easyhtml5video .eh5v_script {
                    display: none
               }

          </style>
          <div class="easyhtml5video is-hidden-touch" style="position:relative;max-width:1920px;">
               <video autoplay="autoplay" poster="video/contest_1920x1080_1.jpg" style="width:100%" title="contest 1920x1080_1" loop="loop"
                    onended="var v=this;setTimeout(function(){v.play()},300)">
                    <source src="video/contest_1920x1080_1.m4v" type="video/mp4" />
                    <source src="video/contest_1920x1080_1.webm" type="video/webm" />
                    <object type="application/x-shockwave-flash" data="video/flashfox.swf" width="1920" height="250" style="position:relative;">
                         <param name="movie" value="video/flashfox.swf" />
                         <param name="allowFullScreen" value="true" />
                         <param name="flashVars" value="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/contest_1920x1080_1.jpg&amp;src=contest_1920x1080_1.m4v"
                         />
                         <embed src="video/flashfox.swf" width="1920" height="250" style="position:relative;" flashVars="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/contest_1920x1080_1.jpg&amp;src=contest_1920x1080_1.m4v"
                              allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en"
                         />
                         <img alt="contest 1920x1080_1" src="video/contest_1920x1080_1.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser"
                         />
                    </object>
               </video>

          </div>
          <!-- End EasyHtml5Video.com BODY section -->



          <!-- Start EasyHtml5Video.com BODY section -->
          <style type="text/css">
               .easyhtml5video .eh5v_script {
                    display: none
               }

          </style>
          <div class="easyhtml5video is-hidden-desktop" style="position:relative;max-width:1080px;">
               <video controls="controls" poster="video/contest_1080x1080.jpg" style="width:100%" title="contest 1080x1080" loop="loop"
                    onended="var v=this;setTimeout(function(){v.play()},300)">
                    <source src="video/contest_1080x1080.m4v" type="video/mp4" />
                    <source src="video/contest_1080x1080.webm" type="video/webm" />
                    <object type="application/x-shockwave-flash" data="video/flashfox.swf" width="1080" height="1080" style="position:relative;">
                         <param name="movie" value="video/flashfox.swf" />
                         <param name="allowFullScreen" value="true" />
                         <param name="flashVars" value="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/contest_1080x1080.jpg&amp;src=contest_1080x1080.m4v"
                         />
                         <embed src="video/flashfox.swf" width="1080" height="1080" style="position:relative;" flashVars="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=video/contest_1080x1080.jpg&amp;src=contest_1080x1080.m4v"
                              allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en"
                         />
                         <img alt="contest 1080x1080" src="video/contest_1080x1080.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser"
                         />
                    </object>
               </video>
               <!-- End EasyHtml5Video.com BODY section -->
     </section>

     </div>

     @include('partials.pricing') 
     
     <section class="section">
       @include('partials.security')
     </section>
   
     @endsection
