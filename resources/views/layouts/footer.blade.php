@if(session()->has('updated')) @include('partials.notification' , ['msg' => session('updated')]) @endif
<footer class="footer main-footer">

     <div class="container">
          <div class="content ">
               <h2 class="title is-4 has-text-centered has-text-weight-bold">Your Legacy, For Generations to Come.</h2>
               
               <div class="columns is-mobile has-text-centered">
                    <div class="column is-6 ">

                         <img src="{{ asset('imgs/google_play.png') }}" class="is-pulled-right footer-brand">

                    </div>
                    <div class="column is-6">

                         <img src="{{ asset('imgs/appstore.png') }}" class=" is-pulled-left footer-brand">

                    </div>
               </div>
               <div class="columns is-mobile flex-columns-mobile has-text-centered">
                    <div class="column is-3-desktop is-12-mobile">
                         <a target="_blank" href="https://www.facebook.com/eirloom/" class="icon socicon-facebook"> </a>
                         <a target="_blank" href="https://twitter.com/_eirloom" class="icon socicon-twitter"> </a>
                         <a target="_blank" href="https://www.instagram.com/eirloom" class="icon socicon-instagram"> </a>
                         <a target="_blank" href="https://digital-legacy.mn.co/share/D8-nPulBVPNCHFaA?utm_source=manual" class="icon socicon-mighty"> </a>
                    </div>
                    <div class="column is-6-desktop is-12-mobile">

                         @include('partials.sitemap')
                    </div>
                    <!-- <div class="column is-3-desktop is-12-mobile">
                         <p class="subtitle is-5">Join Our Mail List</p>
                         <form style="justify-content: center;" class="field has-addons" action="{{ route('mailing') }}" method="POST">
                              @csrf
                              <div class="control">
                                   <input class="input {{ $errors->has('mailing_email') ? ' is-danger' : '' }}" name="mailing_email" type="email" placeholder="email"> @if ($errors->has('mailing_email'))
                                   <p class="help is-danger">{{ $errors->first('mailing_email') }}</p>
                                   @endif
                              </div>
                              <div class="control">
                                   <button type="submit" class="button is-primary">
                                        Join
                                   </button>
                              </div>
                         </form>
                    </div> -->
               </div>
               <div class="columns is-mobile">
                    <div class="column has-text-centered is-12 ">
                         <br>
                         <p>2018 eirloom.com LLC . all Right Reserved</p>
                    </div>
               </div>
          </div>
     </div>
</footer>
