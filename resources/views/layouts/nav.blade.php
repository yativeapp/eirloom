<div class="hero-head">
     <header class="navbar">
          <div class="container">
               <div class="navbar-brand">
                    <a class="navbar-item" href="/">
                         <img src="{{ asset('imgs/logo.png') }}" alt="eirloom">
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenuHeroC">
                         <span></span>
                         <span></span>
                         <span></span>
                    </span>
               </div>
               <div id="navbarMenuHeroC" class="navbar-menu">
                    <!-- Authentication Links -->
                    <div class="navbar-end">
                         @guest
                         <a class="navbar-item {{ active('login' , 'is-active') }}" href="{{ route( 'login') }} ">{{ __('Login') }}</a>
                         <a class="navbar-item {{ active('register' , 'is-active') }} " href="{{ route( 'register') }} ">{{ __('Register') }}</a>
                         @else
                         <a class="navbar-item {{ active('profile' , 'is-active') }} " href="{{ route( 'profile') }} ">Profile</a>
                         <a class="navbar-item {{ active('account' , 'is-active') }} " href="{{ route( 'account') }} ">Account</a>
                         <a class="navbar-item {{ active('recievers' , 'is-active') }} " href="{{ route( 'recievers') }} ">Recievers</a>
                         <span class="navbar-item ">
                              <form id="logout-form" action="{{ route( 'logout') }} " method="POST" style="display: none;">
                                   @csrf
                              </form>
                              <a class="button is-danger is-inverted " href="{{ route( 'logout') }}" onclick="event.preventDefault();
                                                                                          document.getElementById('logout-form').submit(); ">
                                   {{ __('Logout') }}
                              </a>
                         </span>
                         @endguest
                    </div>
               </div>
          </div>
     </header>
</div>
