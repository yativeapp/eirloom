<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?9ukd8d">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet"> @yield('styles')

    <style>
      .main-footer {
        background-image: url('imgs/footer-bg.png');
      }

      .sec-brand {
        height: 5rem;
      }
      .read-more{
        white-space: nowrap; 
    width: 70%; 
    overflow: hidden;
    text-overflow: ellipsis; 
      }
      .slick-list{
        width:100%;
        padding-bottom: 20px;
      }

      .socicon-mighty{
        background-size: contain;
        background-image: url('/imgs/mighty.png')
      }
      .socicon-mighty::before{
        content:'m';
        color:transparent;
      }

              .hero.is-light .subtitle{
            color:#000 !important;
          }

    </style>


  </head>

  <body>
    <section class="hero {{ isset($white) ? '' : 'is-light' }} is-fullheight">
      <!-- Hero head: will stick at the top -->
      @include('layouts.nav')

      <!-- Hero content: will be in the middle -->
      <div class="hero-body {{ isset($full) ? 'pd-bottom' : '' }}">
        @yield('content')
      </div>
    </section>

    @include('layouts.footer') @yield('scripts')

        @if(auth()->check() && session()->has('wasRestored'))
       <script>
         swal("Your Account Has Been Restored!", "Welcome Back {{ auth()->user()->firstname }} ", "success");

       </script>

           @php session()->forget('wasRestored'); @endphp
    @endif


  </body>

</html>
