@extends('layouts.app' , ['full' => true]) 

@section('title' , $user->username)
@section('content') 

@section('styles')
<link href="{{ asset('css/jquery.atwho.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/timeline.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/colorbox.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<link rel="stylesheet" href="{{ asset('css/earthviewer_styles_unique.min.css') }}"> @endsection @section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{ env('GOOGLE_KEY') }}"></script>



<script src="{{ mix('js/profile.js') }} " defer></script>

<script src="{{ asset('js/timeline.min.js') }} " defer></script>
<script src="{{ asset('js/globe.min.js') }} " defer></script> @endsection @if(session()->has('updated')) @include('partials.notification' , ['msg' => session('updated')]) @endif

<div id="app">
<div id="profile-header ">
    <profile-header :user="{{ json_encode($user) }}" :d="{{ $disabled }}"></profile-header>

</div>

<div class="container" style="margin-top: 8em;">
    @include('partials.event.add') @if($events->count()) @include('partials.event.timeline') @include('partials.event.globe')
    @endif @include('partials.event.wisdom')
</div>
</div>
@endsection
