@extends('layouts.app' , ['full' => true]) @section('title' , $user->name) @section('styles')
<link href="{{ asset('css/timeline.min.css') }}" rel="stylesheet">
<link href="http://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/colorbox.css') }}">
<link rel="stylesheet" href="{{ asset('css/earthviewer_styles_unique.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

 @endsection @section('scripts')
<script src="{{ mix('js/profile.js') }} " defer></script>
<script src="{{ asset('js/timeline.min.js') }} " defer></script>
<script src="{{ asset('js/globe.min.js') }} " defer></script>
<script src="http://vjs.zencdn.net/6.6.3/video.js"></script>
@endsection @section('content')
<div id="app">
     <profile-header :user="{{ json_encode(auth()->user()) }}" :disabled="true"></profile-header>

</div>

<div class="container"  style="margin-top: 8em;">
	@if($user->video)
     @include('partials.event.video')
@endif
      @if($events->count()) @include('partials.event.timeline') @include('partials.event.globe')
     @endif @include('partials.event.wisdom' , ['lock' => true])
</div>
@endsection
