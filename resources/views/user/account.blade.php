@extends('layouts.app') @section('title' , 'Account') @section('content') @if(session()->has('updated')) @include('partials.notification'
, ['msg' => session('updated')]) @endif @section('styles')

<link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?9ukd8d"> 

<script src="{{ mix('js/Account.js') }}" defer></script>



@endsection



<div class="container">
     <section class="section box has-text-centered">

          <h3 class="title is-3"> Profile</h3>


          <form action="{{ route('basic.change') }}" method="POST">

               @csrf
               <div class="level">
                    <div class="level-left">
                         <div class="level-item">
                         </div>
                    </div>
                    <div class="level-right">
                         <div class="level-item">
                              <button class="button is-primary" type="submit">Save</button>
                         </div>
                    </div>
               </div>
               <div class="field is-horizontal">
                    <div class="field-body">
                         <div class="field">
                              <label class="label">First Name</label>
                              <p class="control is-expanded">
                                   <input class="input {{ $errors->has('firstname') ? ' is-danger' : '' }}" type="text" name="firstname" value="{{ $user->firstname }}">
                              </p>

                              @if ($errors->has('firstname'))
                              <p class="help is-danger">{{ $errors->first('firstname') }}</p>
                              @endif

                         </div>
                         <div class="field">
                              <label class="label">Last Name</label>
                              <p class="control is-expanded">
                                   <input class="input {{ $errors->has('lastname') ? ' is-danger' : '' }}" type="text" name="lastname" value="{{ $user->lastname }}">
                              </p>
                              @if ($errors->has('lastname'))
                              <p class="help is-danger">{{ $errors->first('lastname') }}</p>
                              @endif
                         </div>
                    </div>
               </div>
               <div class="field">
                    <p class="control">
                         <label class="label">Email</label>
                         <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email" value="{{ $user->email }}">
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
               </div>
               <div class="field">
                    <p class="control">
                         <label class="label">Phone Number</label>
                         <input class="input {{ $errors->has('phonenumber') ? ' is-danger' : '' }}" type="text" name="phonenumber" value="{{ $user->phonenumber }}">
                    </p>
                    @if ($errors->has('phonenumber'))
                    <p class="help is-danger">{{ $errors->first('phonenumber') }}</p>
                    @endif
               </div>

          </form>

          <hr>

          <form action="{{ route('password.change') }}" method="POST">
               @csrf

               <div class="field">
                    <p class="control">
                         <label class="label">Old Password</label>
                         <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password" name="password">
                    </p>
                    @if ($errors->has('password'))
                    <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
               </div>


               <div class="field">
                    <p class="control">
                         <label class="label">New Password</label>
                         <input class="input {{ $errors->has('new_password') ? ' is-danger' : '' }}" type="password" name="new_password">
                    </p>
                    @if ($errors->has('new_password'))
                    <p class="help is-danger">{{ $errors->first('new_password') }}</p>
                    @endif
               </div>

               <div class="level">
                    <div class="level-left">
                         <div class="level-item">
                         </div>
                    </div>
                    <div class="level-right">
                         <div class="level-item">
                              <button class="button is-primary" type="submit">Change</button>
                         </div>
                    </div>
               </div>
          </form>
     </section>


     <div class="box has-text-centered">

          <h3 class="title is-3"> Plan</h3>

          <div class="field">
               <p class="control">
                    <label class="label">current Plan</label>

                {{ $user_plan}}
               </p>
          </div>

          <form class="field has-addons" method="POST" action="{{ route('plan.change') }}">
               @csrf
               <div class="control is-expanded">
                    <div class="select is-fullwidth">
                         <select name="plan">

                              @php $plans = \App\Plans::all() @endphp
                              @foreach($plans as $plan)
                              <optgroup label="{{$plan->name}}">
                              <option value="{{ $plan->name }}-monthly" class="{{ $plan->isSelected($user_plan , 'monthly') }}">{{ $plan->name }}-monthly-{{ $plan->monthly / 100 }}$</option>
                              <option value="{{ $plan->name }}-yearly"  class="{{ $plan->isSelected($user_plan , 'yearly') }}">{{ $plan->name }}-yearly-{{ $plan->yearly / 100 }}$ </option>
                              </optgroup>
                              @endforeach
                         </select>


                    </div>
               </div>
               <div class="control">
                    <button type="submit" class="button is-primary">Change</button>
               </div>
          </form>

     </div>


     <form class="box has-text-centered" method="POST" action="{{ route('billing.change') }}">
          @csrf
          <div class="level">
               <div class="level-left">
                    <div class="level-item">
                    </div>
               </div>
               <div class="level-right">
                    <div class="level-item">
                         <button class="button is-primary" type="submit">Save</button>
                    </div>
               </div>
          </div>
          <h3 class="title is-3">Billing Info</h3>


          <div class="field">
               <p class="control">
                    <label class="label">Phone Number</label>
                    <input class="input {{ $errors->has('phonenumber') ? ' is-danger' : '' }}" type="text" name="phonenumber" value=" {{ $user->phonenumber }}">
               </p>
               @if ($errors->has('phonenumber'))
               <p class="help is-danger">{{ $errors->first('phonenumber') }}</p>
               @endif
          </div>

          <div class="field">
               <p class="control">
                    <label class="label">Address</label>
                    <input class="input {{ $errors->has('address') ? ' is-danger' : '' }}" type="text" name="address" value=" {{ $user->address }}">
               </p>
               @if ($errors->has('address'))
               <p class="help is-danger">{{ $errors->first('address') }}</p>
               @endif
          </div>
          <div class="field">
               <p class="control">
                    <label class="label">Town/City</label>
                    <input class="input {{ $errors->has('city') ? ' is-danger' : '' }}" type="text" name="city" value=" {{ $user->city }}">
               </p>
               @if ($errors->has('city'))
               <p class="help is-danger">{{ $errors->first('city') }}</p>
               @endif
          </div>


          <div class="field is-horizontal">
               <div class="field-body">
                    <div class="field">
                         <label class="label">State</label>
                         <p class="control is-expanded">
                              <input class="input {{ $errors->has('state') ? ' is-danger' : '' }}" type="text" name="state" value=" {{ $user->state }}">
                         </p>

                         @if ($errors->has('state'))
                         <p class="help is-danger">{{ $errors->first('state') }}</p>
                         @endif

                    </div>
                    <div class="field">
                         <label class="label">Zip Code</label>
                         <p class="control is-expanded">
                              <input class="input {{ $errors->has('zip') ? ' is-danger' : '' }}" type="text" name="zip" value=" {{ $user->zip }}">
                         </p>
                         @if ($errors->has('zip'))
                         <p class="help is-danger">{{ $errors->first('zip') }}</p>
                         @endif
                    </div>
               </div>
          </div>

          <hr>

     </form>


     <div class="box has-text-centered">

          <h3 class="title is-3">Card Details</h3>
          <form action="{{ route('card.change') }}" method="POST">
               @csrf
               <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="{{ config('services.stripe.key') }}"
                    data-image="{{ asset('imgs/logo.png') }}" data-email="{{ $user->email }}" data-name="{{ config('app.name') }}"
                    data-panel-label="Update Card Details" data-label="Update Card Details" data-allow-remember-me=false data-locale="auto">


               </script>
          </form>

     </div>


<style>

@media screen and (max-width: 600px) {
  table thead {
    display: none;
  }
  table tr {
    display: block;
    margin-bottom: 10px;
  }
  table td {
    display: block;

    text-align: center;
  }
  table td:before {
    content: attr(data-label);
    float: left;
  }
}

</style>

     @include('partials.trustee') @include('partials.socialWill')



<div id="app">
    <Delete-Account></Delete-Account>
</div>
</div>

@endsection


