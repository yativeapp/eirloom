@extends('layouts.app') @section('title' , 'Recivers') @section('scripts')

<script src="{{ mix('js/receivers.js') }}" defer></script>

@endsection @section('content')

<div class="container" id="app">

     <Receivers :data="{{ $profiles }}"></Receivers>
</div>

@endsection
