@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else

# Hello {{ $user->firstname }}
@endif

{{-- Intro Lines --}}
We Sending You This To Inform you about sad news , please click this button to know more 

{{-- Action Button --}}
@component('mail::button', ['url' => route('reciever.get' , ['id' => $user->id]), 'color' => 'red'])
View Profile
@endcomponent


{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
Regards,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
If you’re having trouble clicking the "View Profile" button, copy and paste the URL below
into your web browser: [route('reciever.get' , ['id' => $user->id]](route('reciever.get' , ['id' => $user->id])
@endcomponent
@endisset
@endcomponent
