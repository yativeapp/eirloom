<div class="box has-text-centered">

     <h3 class="title is-3">Trustee</h3>
     <form action="{{ route('trustee') }}" method="POST">
          @csrf
          <div class="field">
               <p class="control">
                    <label class="label">Name</label>
                    <input class="input {{ $errors->has('trustee_name') ? ' is-danger' : '' }}" type="trustee_name" name="trustee_name" value="{{ isset($t->username) ? $t->username : '' }}">
               </p>
               @if ($errors->has('trustee_name'))
               <p class="help is-danger">{{ $errors->first('trustee_name') }}</p>
               @endif
          </div>
          <div class="field">
               <p class="control">
                    <label class="label">Email</label>
                    <input class="input {{ $errors->has('trustee_email') ? ' is-danger' : '' }}" type="trustee_email" name="trustee_email" value="{{ isset($t->email) ? $t->email : '' }}">
               </p>
               @if ($errors->has('trustee_email'))
               <p class="help is-danger">{{ $errors->first('trustee_email') }}</p>
               @endif
          </div>
          <div class="field">
               <p class="control">
                    <label class="label">Phone Number</label>
                    <input class="input {{ $errors->has('trustee_number') ? ' is-danger' : '' }}" type="trustee_number" name="trustee_number"
                         value="{{ isset($t->phonenumber) ? $t->phonenumber : '' }}">
               </p>
               @if ($errors->has('trustee_number'))
               <p class="help is-danger">{{ $errors->first('trustee_number') }}</p>
               @endif
          </div>
          <div class="level">
               <div class="level-left">
                    <div class="level-item">
                    </div>
               </div>
               <div class="level-right">
                    <div class="level-item">
                         <button class="button is-primary" type="submit">Save</button>
                    </div>
               </div>
          </div>
     </form>
     <hr>

     <form action="{{ route('trustee.release') }}" method="POST">
          @csrf
          <label class="checkbox">
               <input type="checkbox" name="release"> I , the Trustee i understand By Clicking this button below , i will realeaseing the recievers pages . i understand
               that this is only done in the case that the user has passed away
          </label>
          <br>
          <br>
          <button class="button is-warning" type="submit">Release Reciever Pages</button>
     </form>
</div>
