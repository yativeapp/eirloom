<form class="box has-text-centered" method="POST" action="{{ route('will.change') }}">
  @csrf
  <div class="level">
    <div class="level-left">
      <div class="level-item">
      </div>
    </div>
    <div class="level-right">
      <div class="level-item">
        <button class="button is-primary" type="submit">Save</button>
      </div>
    </div>
  </div>
  <h3 class="title is-3">Social Media Will</h3>


  <table class="table is-fullwidth is-hoverable is-striped">
    <thead>
      <tr>
        <td>Channel</td>
        <td>Username / Email</td>
        <td>Password</td>
        <td>Who Should Manage It ?</td>
        <td>What Would You like to be Done ?</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <span class="icon socicon-facebook"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[facebook][name]" value="{{ isset($will['facebook']['name']) ? $will['facebook']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[facebook][password]" value="{{ isset($will['facebook']['password']) ? $will['facebook']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[facebook][email]" value="{{ isset($will['facebook']['email']) ? $will['facebook']['email'] : '' }}">
        </td>
        <td data-label="Messsage">
          <textarea class="textarea" rows="3" name="will[facebook][msg]">{{ isset($will['facebook']['msg']) ? $will['facebook']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
         <td>
          <span class="icon socicon-twitter"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[twitter][name]" value="{{ isset($will['twitter']['name']) ? $will['twitter']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[twitter][password]" value="{{ isset($will['twitter']['password']) ? $will['twitter']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[twitter][email]" value="{{ isset($will['twitter']['email']) ? $will['twitter']['email'] : '' }}">
        </td>
        <td data-label="Messsage">
          <textarea class="textarea" rows="3" name="will[twitter][msg]">{{ isset($will['twitter']['msg']) ? $will['twitter']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-instagram"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[instagram][name]" value="{{ isset($will['instagram']['name']) ? $will['instagram']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[instagram][password]" value="{{ isset($will['instagram']['password']) ? $will['instagram']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[instagram][email]" value="{{ isset($will['instagram']['email']) ? $will['instagram']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[instagram][msg]">{{ isset($will['instagram']['msg']) ? $will['instagram']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-linkedin"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[linkedin][name]" value="{{ isset($will['linkedin']['name']) ? $will['linkedin']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[linkedin][password]" value="{{ isset($will['linkedin']['password']) ? $will['linkedin']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[linkedin][email]" value="{{ isset($will['linkedin']['email']) ? $will['linkedin']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[linkedin][msg]">{{ isset($will['linkedin']['msg']) ? $will['linkedin']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-youtube"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[youtube][name]" value="{{ isset($will['youtube']['name']) ? $will['youtube']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[youtube][password]" value="{{ isset($will['youtube']['password']) ? $will['youtube']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[youtube][email]" value="{{ isset($will['youtube']['email']) ? $will['youtube']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[youtube][msg]">{{ isset($will['youtube']['msg']) ? $will['youtube']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-snapchat"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[snapchat][name]" value="{{ isset($will['snapchat']['name']) ? $will['snapchat']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[snapchat][password]" value="{{ isset($will['snapchat']['password']) ? $will['snapchat']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[snapchat][email]" value="{{ isset($will['snapchat']['email']) ? $will['snapchat']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[snapchat][msg]">{{ isset($will['snapchat']['msg']) ? $will['snapchat']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-googleplus"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[googleplus][name]" value="{{ isset($will['googleplus']['name']) ? $will['googleplus']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[googleplus][password]" value="{{ isset($will['googleplus']['password']) ? $will['googleplus']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[googleplus][email]" value="{{ isset($will['googleplus']['email']) ? $will['googleplus']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[googleplus][msg]">{{ isset($will['googleplus']['msg']) ? $will['googleplus']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-pinterest"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[pinterest][name]" value="{{ isset($will['pinterest']['name']) ? $will['pinterest']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[pinterest][password]" value="{{ isset($will['pinterest']['password']) ? $will['pinterest']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[pinterest][email]" value="{{ isset($will['pinterest']['name']) ? $will['pinterest']['name'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[pinterest][msg]">{{ isset($will['pinterest']['msg']) ? $will['pinterest']['msg'] : '' }}</textarea>
        </td>
      </tr>
      <tr>
        <td data-label="">
          <span class="icon socicon-blogger"> </span>
        </td>
        <td data-label="Username">
          <input class="input" name="will[blogger][name]" value="{{ isset($will['blogger']['name']) ? $will['blogger']['name'] : '' }}">
        </td>
        <td data-label="Password">
          <input class="input" type="text" name="will[blogger][password]" value="{{ isset($will['blogger']['password']) ? $will['blogger']['password'] : '' }}">
        </td>
        <td data-label="Email">
          <input class="input" type="email" name="will[blogger][email]" value="{{ isset($will['blogger']['email']) ? $will['blogger']['email'] : '' }}">
        </td>
        <td data-label="Message">
          <textarea class="textarea" rows="3" name="will[blogger][msg]">{{ isset($will['blogger']['msg']) ? $will['blogger']['msg'] : '' }}</textarea>
        </td>
      </tr>
    </tbody>
  </table>
</form>
