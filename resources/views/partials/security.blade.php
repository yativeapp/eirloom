<h3 class="title is-3 has-text-centered">
  Security is important to us.</h3>

<div class="columns">

  <div class="column flex-all-center">
    <img class="sec-brand" src="{{ asset('imgs/sucuri.png') }}">
  </div>
  <div class="column flex-all-center">
    <img class="sec-brand" src="{{ asset('imgs/le-logo.png') }}">
  </div>
  <div class="column flex-all-center">
    <img class="sec-brand" src="{{ asset('imgs/feature-rounded.png') }}">
  </div>


</div>
