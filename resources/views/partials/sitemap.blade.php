<div class="columns has-text-centered">
     <div class="column">
          <p class="title has-text-weight-bold is-5">About us</p>
          <p>
               <a href="{{ route('company') }}">Company</a>
          </p>
          <p>
               <a target="_blank" href="http://blog.eirloom.com">Blog</a>
          </p>
          <p>
               <a href="{{ url('/')}}#pricing">Pricing</a>
          </p>
          <p>
               <a href="{{ route('faq') }}#tricks">Video Tips & Tricks </a>
          </p>
          <p>
               <a href="{{ route('sitemap') }}">Site Map </a>
          </p>
     </div>
     <div class="column">
          <p class="title has-text-weight-bold is-5">Support and Trust</p>
          <p>
               <a href="{{ route('company') }}">Contact Us</a>
          </p>
          <p>
             <a href="{{ route('faq') }}">Faq </a>  
          </p>
          <p>
             <a href="{{ route('terms') }}">Terms Of Use </a>  
          </p>
          <p>
               <a href="{{ route('terms') }}">Privacy Policy</a>
          </p>
          <p>
               <a href="{{ route('company') }}">Report User's Death  </a>
          </p>
     </div>
</div>
