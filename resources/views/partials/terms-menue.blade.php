<div class="termslist-m">
     <aside class="box menu has-text-centered ">

          <p class="menu-label">
               Guide
          </p>


          <ul class="menu-list terms-list ">

               <li>
                    <a href="#terms" class="is-uppercase"> Terms of use </a>
               </li>
               <li>
                    <a href="#purchases" class="is-uppercase"> Purchases </a>
               </li>
               <li>
                    <a href="#user" class="is-uppercase"> USER REPRESENTATION </a>
               </li>
               <li>
                    <a href="#contr" class="is-uppercase"> CONTRIBUTION LICENSE </a>
               </li>
               <li>
                    <a href="#guide" class="is-uppercase"> GUIDELINES FOR REVIEWS </a>
               </li>
               <li>
                    <a href="#mobile" class="is-uppercase"> MOBILE APPLICATION LICENSE </a>
               </li>
               <li>
                    <a href="#social" class="is-uppercase"> social media </a>
               </li>
               <li>
                    <a href="#submissions" class="is-uppercase"> submissions </a>
               </li>
               <li>
                    <a href="#PROHIBITED" class="is-uppercase"> PROHIBITED ACTIVITIES </a>
               </li>
               <li>
                    <a href="#INTELLECTUAL" class="is-uppercase"> INTELLECTUAL PROPERTY RIGHTS </a>
               </li>
               <li>
                    <a href="#THIRDPARTY" class="is-uppercase"> THIRD PARTY WEBSITES AND CONTENT </a>
               </li>
               <li>
                    <a href="#SITEMANAGEMENT" class="is-uppercase"> SITE MANAGEMENT </a>
               </li>
               <li>
                    <a href="#PRIVACYPOLICY" class="is-uppercase"> PRIVACY POLICY </a>
               </li>
               <li>
                    <a href="#DIGITALMILLENNIUM" class="is-uppercase"> DIGITAL MILLENNIUM COPYRIGHT</a>
               </li>
               <li>
                    <a href="#TERMANDTERMINATION" class="is-uppercase"> TERM AND TERMINATION </a>
               </li>
               <li>
                    <a href="#MODIFICATIONS" class="is-uppercase"> MODIFICATIONS </a>
               </li>
               <li>
                    <a href="#DISPUTES" class="is-uppercase"> DISPUTES </a>
               </li>
               <li>
                    <a href="#CORRECTIONS" class="is-uppercase"> CORRECTIONS </a>
               </li>
               <li>
                    <a href="#DISCLAIMERS" class="is-uppercase"> DISCLAIMERS </a>
               </li>
               <li>
                    <a href="#LIMITATIONS" class="is-uppercase"> LIMITATIONS OF LIABILITY </a>
               </li>
               <li>
                    <a href="#INDEMNITY" class="is-uppercase"> INDEMNITY </a>
               </li>
               <li>
                    <a href="#NOTICES" class="is-uppercase"> NOTICES </a>
               </li>
               <li>
                    <a href="#USERDATA" class="is-uppercase"> USER DATA </a>
               </li>
               <li>
                    <a href="#CONTRACTING" class="is-uppercase"> CONTRACTING AND SIGNATURES </a>
               </li>
               <li>
                    <a href="#MISCELLANEOUS" class="is-uppercase"> MISCELLANEOUS </a>
               </li>
               <li>
                    <a href="#CONTACT" class="is-uppercase"> CONTACT </a>
               </li>
          </ul>

     </aside>
</div>
