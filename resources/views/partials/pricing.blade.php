<div class="pricing-table is-comparative" id="pricing">
  <div class="pricing-plan is-features">
    <div class="plan-header">Features</div>
    <div class="plan-price"><span class="plan-price-amount">&nbsp;</span></div>
    <div class="plan-items">
      <div class="plan-item">Reciever Profiles</div>
      <div class="plan-item">Total Space</div>
      <div class="plan-item">Years Kept After</div>
    </div>
    <div class="plan-footer">

    </div>
  </div>
  <div class="pricing-plan">
    <div class="plan-header">Basic</div>
    <div class="plan-price"><span class="plan-price-amount">
    <span class="plan-price-currency">$</span>4.99</span>/month 
     or 50$ Yearly
    </div>
        <div class="plan-items">
      <div class="plan-item" data-feature="Reciever Profiles">3</div>
      <div class="plan-item" data-feature="Total Space">25 GB</div>
      <div class="plan-item" data-feature="Years Kept After">10</div>
    </div>
    <div class="plan-footer">
            <a href="register?plan=basic&term=monthly" class="button is-fullwidth">Monthly</a>
            <a href="register?plan=basic&term=yearly" class="button is-fullwidth">Yearly</a>
    </div>
  </div>

  <div class="pricing-plan is-warning is-active">
    <div class="plan-header">Premuim</div>
    <div class="plan-price"><span class="plan-price-amount">
    <span class="plan-price-currency">$</span>6.99</span>/month 
     or 70$ Yearly
    </div>
    <div class="plan-items">
      <div class="plan-item" data-feature="Reciever Profiles">6</div>
      <div class="plan-item" data-feature="Total Space">50 GB</div>
      <div class="plan-item" data-feature="Years Kept After">15</div>
    </div>
    <div class="plan-footer">
      <a href="register?plan=premuim&term=monthly" class="button is-fullwidth">Monthly</a>
      <a href="register?plan=premuim&term=yearly" class="button is-fullwidth">Yearly</a>
    </div>
  </div>

  <div class="pricing-plan ">
    <div class="plan-header">Ultimate</div>
    <div class="plan-price"><span class="plan-price-amount">
    <span class="plan-price-currency">$</span>9.99</span>/month 
     or 99$ Yearly
    </div>
        <div class="plan-items">
      <div class="plan-item" data-feature="Reciever Profiles">9</div>
      <div class="plan-item" data-feature="Total Space">100 GB</div>
      <div class="plan-item" data-feature="Years Kept After">20</div>
    </div>
    <div class="plan-footer">
      <a href="register?plan=ultimate&term=monthly" class="button is-fullwidth">Monthly</a>
      <a href="register?plan=ultimate&term=yearly" class="button is-fullwidth">Yearly</a>
    </div>
  </div>

</div>