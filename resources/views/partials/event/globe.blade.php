<section class="section">
     <div id="globeGl_" class="globeGl"></div>

     @foreach($events as $event)
     <div class="hotspot hidden" data-lat="{{ $event->lat }}" data-long="{{ $event->lng }}" data-url="" data-urltarget="_blank"
          data-hotspotclass="hotspotoverride" data-hotspoticon="{{ asset('textures/hotspotIconsGreen/micro_LM.png') }}" data-hotspotalign="LT"
          data-clickexternal="" data-headtxt="">
          {{ $event->date->format('jS \of F Y') }}
          <br> {{ $event->title }}<br>

  <a   data-fancybox="gallery" href="{{ url($event->attachment) }}">
    <figure class="image is-4by3">
      <img src="{{ $event->isVideo() ? asset('imgs/video_placeholder.png') : url($event->attachment)}}">
    </figure>
</a>
     </div>
     @endforeach

</section>
