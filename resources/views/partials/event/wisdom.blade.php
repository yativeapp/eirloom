<form class="section box" action="{{ route('profile.wisdom') }}" method="POST" enctype="multipart/form-data">
     @csrf
     <h3 class="title is-3">Words Of Wisdom</h3>

     @if(isset($lock))
     <div class="content">
          @foreach($user->owner->wisdomAsList() as $w)
       <blockquote>{{ $w }}</blockquote>
       @endforeach
     </div>

     @else
     <div class="field">
          <p class="control">
               <textarea class="textarea" rows="10" required name="wisdom">{{auth()->user()->wisdom}} </textarea>
          </p>
     </div>
     <button class="button is-success" type="submit">Update</button>
     @endif

</form>
