<div class="box">
     <video id="my-video" class="video-js" controls preload="auto" style="width: 100%;" poster="{{ $user->thumb }}" data-setup="{}">
          <source src="/storage/videos/{{ $user->video }}" type='video/mp4'>
          <p class="vjs-no-js">
               To view this video please enable JavaScript, and consider upgrading to a web browser that
               <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
          </p>
     </video>
     <br>
     <a href="/recievers/{{ $user->id }}/download" class="button is-success">Download</a>
</div>
