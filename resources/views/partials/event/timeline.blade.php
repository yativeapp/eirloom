<section class="section box">

<div class="steps  slider-for">
  @foreach($events as $event)
      <div class="step-item  is-blue">
    <div class="step-marker ">
      {{ $event->date->format('jS \of F Y') }}
    </div>
    </div>
  @endforeach
</div>


<div class="timeline">

@foreach($events as $event)

<div>
  <div class="card">
  <a class="card-image" data-fancybox="gallery" href="{{ url($event->attachment) }}">
    <figure class="image is-4by3">
      <img src="{{ $event->isVideo() ? url($event->thumb) : url($event->attachment)}}">
    </figure>
</a>
  <div class="card-content">
    <div class="media">
      <div class="media-content">
        <div class="level">
          <div class="level-left">
            <div class="level-item">
              <p class=" read-more title is-4">{{ $event->title }}</p>
              @if($event->isVideo())
                 <span class="tag" style="margin-left:.4em">
                    Video
                 </span>
              @endif
            </div>
          </div>
          <div class="level-right">
            <div class="level-item">
                <form action="{{ route('event.delete') }}" method="post">
                  @csrf
                  <input type="hidden" name="id" value="{{ $event->id}}">
                  <button type="submit" class="button is-danger is-small">Delete</button>
                </form>
            </div>
          </div>
        </div>
        <p class="read-more">
          {{ $event->place }}
          </p>

      </div>
    </div>

    <div class="content">
      {{ $event->msg }}
      <br>

    </div>
  </div>
    <footer class="card-footer">
    <div class="card-footer-item">
    <div class="tags">
  {!! $event->people !!}
</div>
    </div>

  </footer>
</div>
</div>

@endforeach
</div>
    

</section>

