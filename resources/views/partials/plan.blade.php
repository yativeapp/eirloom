<aside class="box menu has-text-centered">
    <p class="menu-label">
        Your Membership Selection
    </p>
        <div class="select is-small">
        
  <select name="term" autocomplete="off">
    <option value="monthly"{{ $term == 'monthly' ? 'selected' : '' }}>Monthly</option>
    <option value="yearly" {{ $term == 'yearly' ? 'selected' : '' }} >Yearly</option>
  </select>
</div>
    <ul class="menu-list {{ $selectable ? 'subscribtion-list' : ''}} ">

        @foreach($plans as $plan)
        <li>
            <a class="{{ $planl == $plan->name ? 'is-active' : '' }}" data-plan="{{ $plan->name }}">
                {{ $plan->name }}
            </a>
        </li>
        @endforeach
    </ul>

<br>

  <p class="control">
     <input class="input is-small" type="text" name="coupon" placeholder="Coupon" value="{{ session()->has('prereg') ? session('prereg')['coupon'] : '' }}">
  </p>

    <br>
    <p>Select different plan.</p>
    <hr>

    <h3 class="title is-4 is-color-blue">7 Days Free</h3>
    <br>
    <h4 class="subtitle is-5 ">Pay $0 Today</h4>

    <hr>

    <h4 class="title is-5">After your 7-day free trial</h4>
    <br>
    <p class="subtitle is-6 ">After your 7-day free trial ends, your paid subscription will begin and you will be charged the full amount of your membership
        , unless you cancel on or before (youre trail end date).</p>

    <hr>

    <h4 class="title is-5">Automatic Renewal</h4>
    <br>
    <p class="subtitle is-5 ">Your subscription will automatically renew every one month (or Year) and your payment method will be charged at the standard
        price for your chosen subscription. If you wish to end your subscription, simply cancel via the instructions below.</p>

    <hr>

    <h4 class="title is-5">Cancellation</h4>
    <br>
    <p class="subtitle is-6 ">To cancel your subscription, visit the "Your Account" page or go to the
        <a href="{{ route('company') }}">Contact Us Page.</a>
    </p>


    <hr>
    <p class="subtitle is-6 ">I understand that by clicking below I will be starting a free trial.</p>



</aside>
