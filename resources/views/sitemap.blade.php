@extends('layouts.app') @section('title' , 'Site Map') @section('content')

<div class="container" id="app">

     <div class="box">
          <div class="content">

               @include('partials.sitemap')
          </div>
     </div>
</div>

@endsection
