@extends('layouts.app') @section('title' , 'Company') @section('content')

<div class="container">


    <section class="section">
        <h3 class="title is-3">About Us</h3>

        <p>
eirloom.com is a family owned company by the Salmon Family. Currently living in Las Vegas, Nv the Salmon Family consists of Timothy, Brittany, The energetic princess Emery and their newest guppy Westyn. The idea for eirloom.com came to Timothy after waking up from a dream in which he dreamt he passed away and had to watch Emery grow up without him. When he woke up he laid there, watching Emery sleep, and thought he had to find a way to ensure she knew he loved her no matter what tomorrow brings. Living through losses of their own (Timothy’s best friend and Little Brother, and Brittany’s Mother and best friend) they knew how hard it could be for those left behind, and they came up with eirloom.com. It’s a way to give some light during a dark time. A way for the loved ones to get closure, and the members get a piece of mind.
        </p>

    </section>

    <section class="section">

        <div class="columns">

            <div class="column">
                <h3 class="title is-3">Contact Us</h3>
                <p>
                    Thank you for being an eirloom.com member. If you need assistance or help please fill out the form next to this. Once you
                    hit submit you will receive an email confirming we received your information. If this is to report a
                    user’s passing, please include as much information about the user as you can. Including full legal name
                    and current email. Please be patient with this family owned company and around 48 hours to hear back.
                    Thank you.
                </p>
            </div>
            <form class="column" action="{{ route('contact') }}" method="post">

                @csrf
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <label class="label">First Name</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('firstname') ? ' is-danger' : '' }}" type="text" name="firstname">
                            </p>

                            @if ($errors->has('firstname'))
                            <p class="help is-danger">{{ $errors->first('firstname') }}</p>
                            @endif

                        </div>
                        <div class="field">
                            <label class="label">Last Name</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('lastname') ? ' is-danger' : '' }}" type="text" name="lastname">
                            </p>
                            @if ($errors->has('lastname'))
                            <p class="help is-danger">{{ $errors->first('lastname') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="field">
                    <p class="control">
                        <label class="label">Email</label>
                        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email">
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <label class="label">Phone Number</label>
                        <input class="input {{ $errors->has('phonenumber') ? ' is-danger' : '' }}" type="text" name="phonenumber">
                    </p>
                    @if ($errors->has('phonenumber'))
                    <p class="help is-danger">{{ $errors->first('phonenumber') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <label class="label">How May we Help You ?</label>
                        <textarea class="textarea {{ $errors->has('msg') ? ' is-danger' : '' }}" rows="10" name="msg"></textarea>
                    </p>
                    @if ($errors->has('msg'))
                    <p class="help is-danger">{{ $errors->first('msg') }}</p>
                    @endif
                </div>

                <div class="level"></div>
                <div class="level-left">
                    <div class="level-item">
                        <br>

                    </div>
                </div>
                <div class="level-right">
                    <div class="level-item">
                        <button class="button" type="submit">Submit</button>
                    </div>
                </div>
        </div>



        </form>
</div>
</section>
</div>
@endsection
