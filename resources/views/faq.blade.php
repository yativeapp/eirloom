@extends('layouts.app') 

@section('title' , 'FAQ')


@section('content')


<div class="content">
<h1 class="has-text-centered">FAQ</h1>

  <h1>What is eirloom?</h1>
  <p>eirloom is a digital legacy site that promotes the passing on of love and life. </p>

  <h1>What can I do on eirloom.com?</h1>
  <p>On eirloom.com users can create receiver videos, that if they are to pass suddenly, will get released by a trustee to the people the pages are made for. The user
will also be able to create a timeline of photos and information, and a map of locations they have been to with notes, both will be added to receiver pages. </p>
  
  <h1>Why the name “eirloom”?</h1>
  <p>It’s a way to combine the term “electronic heirloom” in an easy to remember fashion. The word heirloom is pronounced /‘erloom/ so we felt it was a natural way
of putting it together.</p>

  <h1>Why have a lower case e?</h1>
  <p>Frankly, it looks better in logos.</p>

  <h1>How do I change my account info?</h1>
  <p>On your user page is an account button on the top right. Once in your account info press edit.</p>
  
  <h1>How do I upgrade my account?</h1>
<p>In the account page it displays your current plan. Click change next to it and select your new plan.</p>
 
  <h1>What is the difference between the site and the app?</h1>
  <p>None. Well, not really. The app is linked to the site so anything changed on one updates in the other. </p>
  
  <h1>I’m having problems Getting into the site, what should I do?</h1>
  <p>Please contact us through email. We will look into any issues and solve them as quickly as we can.</p>
  
  <h1>Do I need a Trustee?</h1>
  <p>Yes. You need someone that you can trust and give some information about this to that will be responsible in case you pass away. As of right now the only way
for the videos to be released is by your trustee signing in and hitting the release button. This person must be responsible and should keep your log-in information
safe. If for any reason they loose it we will be able to help them after they contact customer service.</p>

  <h1>Are there any safeguards in case my trustee forgets?</h1>
  <p>Costumer service we can help them through the process, but only after receiving proof they are who they say they are. We will continue to always look for better
and more efficient ways to handle the sensitive material.
</p>

  <h1>Can I make a receiver page for Social Media?</h1>
  <p>Yes of course. </p>


  <p>just remember, if your video is being released that means you have passed and people will be mourning your loss, if
you make ANY video, please keep it kind and pass on love.</p>


<p>If you have any other questions reach out to us at contact@eirloom.com</p>




<h1 class="has-text-centered" id="tricks">Video Tips & Tricks</h1>

<h1>Shooting:</h1>

<ul>
<li>	If talking to the Camera line yourself up Center. </li>
<li>	Ensure that your face is lit well. </li>
<li>	Do not shoot with the window or sun behind you.</li>
<li>	Set Camera on a steady object if you don’t have access to a tripod. (Try to avoid holding it)</li>
<li>	Make sure focus is set on the main object, you.</li>
<li>	Try to use a Mid shot. That is a shot that shows a little room above your head, and the bottom ends at the middle of your chest.</li>
<li>	Shoot your video in Landscape not Portrait Orientation. This means, if on your phone, shoot long was and not tall. </li>
</ul>


<h1>Editing:</h1>

<ul>
<li>	If you shoot in Portrait Orientation you can lay it on top of itself, make the background one large enough to covers the sides and</li>
	add a blur to make it not the focus.</li>
<li>	Use straight cuts and fades. Try to avoid strange wipes and weird flashy transitions.</li>
<li>	If there is a jump cut or mistake you desire to cover up use a photo that relates to what you are talking about.</li>
</ul>

</div>


@endsection