@extends('layouts.app') @section('title' , 'Forget Password') @section('content')
<div class="container">
    <div class="columns">

        <div class="column is-half is-offset-one-quarter box">
            <h3 class="title is-3">Reset Your Account Password</h3>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="field">
                    <p class="control">
                        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email" placeholder="Email Address">
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>

                <div class="level">
                    <div class="level-right">
                        <div class="level-item">
                            <button class="button is-warning" type="submit">
                                {{ __('Send Password Reset Link') }}

                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection
