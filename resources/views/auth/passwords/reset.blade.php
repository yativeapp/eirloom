@extends('layouts.app') @section('title' , 'Reset Password') @section('content')
<div class="container">
    <div class="columns">

        <div class="column is-half is-offset-one-quarter box">
            <h3 class="title is-3">{{ __('Reset Password') }}</h3>
            <form method="POST" action="{{ route('password.request') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="field">
                    <p class="control">
                        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email" placeholder="Email Address"
                            required autofocus>
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <input id="password" type="password" class="input {{ $errors->has('password') ? ' is-danger' : '' }}" name="password" placeholder="Password"
                            required>
                    </p>
                    @if ($errors->has('password'))
                    <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <input id="password-confirm" type="password" class="input {{ $errors->has('password_confirmation') ? ' is-danger' : '' }}"
                            name="password_confirmation" placeholder="Re Password" required>
                    </p>
                    @if ($errors->has('password_confirmation'))
                    <p class="help is-danger">
                        {{ $errors->first('password_confirmation') }}
                    </p>
                    @endif
                </div>

                <div class="level">
                    <div class="level-right">
                        <div class="level-item">
                            <button class="button is-warning" type="submit">
                                {{ __('Reset Password') }}

                            </button>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection
