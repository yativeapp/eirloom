@extends('layouts.app') @section('title' , 'Checkout') @section('content') @if(session()->has('payment_failed')) @include('partials.notification',
['msg' => session('payment_failed')]) @endif


<div class="columns">

    <div class="column is-9">

        <div class="box">
            <h3 class="title is-3">Account Overview</h3>
            <p class="subtitle is-6">You can create you accont</p>

            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <label class="label">First Name</label>
                        <p>{{ $data['firstname'] }}</p>

                    </div>
                    <div class="field">
                        <label class="label">Last Name</label>
                        <p>{{ $data['lastname'] }}</p>
                    </div>
                </div>
            </div>


            <div class="field">
                <label class="label">Email</label>
                <p>{{ $data['email'] }}</p>
            </div>

        </div>



        <div class="box">
            <h3 class="title is-3">Order Summary</h3>
            <div class="field">
                <label class="label">Choosen Plan</label>
                <p>{{ $data['plan'] }}</p>
            </div>
            <div class="field">
                <label class="label">Plan Cycle</label>
                <p>{{ $data['term'] }}</p>
            </div>
            <div class="field">
                <label class="label">Start Date Of First Payment</label>
                <p>{{ now()->format('jS \of F Y') }}</p>
            </div>


            <hr>

@if($off > 0)
            <div class="notification is-success">

            You Now Has <strong>{{ $off }}% </strong> off with <strong>{{ $data['coupon'] }}</strong> Coupon , Enjoy
</div>
@endif
            <form action="{{ route('register') }}" method="POST">
                @csrf
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="{{ config('services.stripe.key') }}"
                    data-amount="{{ $plan_price }}" data-name="{{ config('app.name') }}" data-panel-label="Pay and Register"
                    data-description="subscription" data-email="{{ $data['email'] }}" data-image="{{ asset('imgs/logo.png') }}"
                    data-locale="auto">


                </script>
            </form>

        </div>

    </div>
    <div class="column">
        @include('partials.plan' , ['selectable' => false , 'planl' => $data['plan'] , 'term' => $data['term'] ])
    </div>
</div>
@endsection
