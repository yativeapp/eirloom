@extends('layouts.app') @section('title' , 'Register') @section('content')

<div class="container">


    <form class="columns" action="{{ route('prereg') }}" method="POST">
        @csrf

        <input type="hidden" name="plan" value="{{ request()->has('plan') ? request('plan'): 'basic' }}">
        <div class="column is-9">

            <div class="box">
                <h3 class="title is-3">Creat Account</h3>
                <p class="subtitle is-6">You can create you accont</p>

                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <label class="label">First Name</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('firstname') ? ' is-danger' : '' }}" type="text" name="firstname">
                            </p>

                            @if ($errors->has('firstname'))
                            <p class="help is-danger">{{ $errors->first('firstname') }}</p>
                            @endif

                        </div>
                        <div class="field">
                            <label class="label">Last Name</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('lastname') ? ' is-danger' : '' }}" type="text" name="lastname">
                            </p>
                            @if ($errors->has('lastname'))
                            <p class="help is-danger">{{ $errors->first('lastname') }}</p>
                            @endif
                        </div>
                    </div>
                </div>


                <div class="field">
                    <p class="control">
                        <label class="label">Email</label>
                        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email">
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>


                <div class="field">
                    <p class="control">
                        <label class="label">Password</label>
                        <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password" name="password">
                    </p>
                    @if ($errors->has('password'))
                    <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>


                <div class="field">
                    <p class="control">
                        <label class="label">Re - Password</label>
                        <input class="input {{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" type="password" name="password_confirmation">
                    </p>
                    @if ($errors->has('password_confirmation'))
                    <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                </div>

            </div>



            <div class="box">
                <h3 class="title is-3">Billing Info</h3>
                <p class="subtitle is-6">You can create you accont</p>


                <div class="field">
                    <p class="control">
                        <label class="label">Phone Number</label>
                        <input class="input {{ $errors->has('phonenumber') ? ' is-danger' : '' }}" type="text" name="phonenumber">
                    </p>
                    @if ($errors->has('phonenumber'))
                    <p class="help is-danger">{{ $errors->first('phonenumber') }}</p>
                    @endif
                </div>

                <div class="field">
                    <p class="control">
                        <label class="label">Address</label>
                        <input class="input {{ $errors->has('address') ? ' is-danger' : '' }}" type="text" name="address">
                    </p>
                    @if ($errors->has('address'))
                    <p class="help is-danger">{{ $errors->first('address') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <label class="label">Town/City</label>
                        <input class="input {{ $errors->has('city') ? ' is-danger' : '' }}" type="text" name="city">
                    </p>
                    @if ($errors->has('city'))
                    <p class="help is-danger">{{ $errors->first('city') }}</p>
                    @endif
                </div>


                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <label class="label">State</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('state') ? ' is-danger' : '' }}" type="text" name="state">
                            </p>

                            @if ($errors->has('state'))
                            <p class="help is-danger">{{ $errors->first('state') }}</p>
                            @endif

                        </div>
                        <div class="field">
                            <label class="label">Zip Code</label>
                            <p class="control is-expanded">
                                <input class="input {{ $errors->has('zip') ? ' is-danger' : '' }}" type="text" name="zip">
                            </p>
                            @if ($errors->has('zip'))
                            <p class="help is-danger">{{ $errors->first('zip') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <hr>
                <label class="checkbox">
  <input type="checkbox" name="agree">
  By Clicking this button you agree on terms mentioned in <a href="{{ route('terms') }}">Terms </a> Page
</label>
@if ($errors->has('agree'))
                            <p class="help is-danger">{{ $errors->first('agree') }}</p>
                            @endif
                            <br>
                            <br>
                <button class="button is-success" type="submit">Countinue</button>
            </div>


        </div>
        <div class="column">

            @include('partials.plan' , ['selectable' => true , 'planl' => request()->has('plan') ? request('plan'): 'basic' , 'term' => request('term') , 'cont' => true])

        </div>
    </form>

    @include('partials.security')
</div>

@endsection
