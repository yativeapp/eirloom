@extends('layouts.app') @section('title' , 'Login') @section('styles')

<style>

    .hero::before{
        
        content:'';
        background-image: url("{{ asset('imgs/login-bg.png') }}");


    /* Full height */
    height: 100%; 

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: absolute;
    width: 100%;
    }

    .login {
        background-image: url("{{ asset('imgs/login-lock.png') }}");
        padding: 30px;
        background-size: 300%;
        background-position: -127px 5px;
        background-repeat: no-repeat;
        border: none;
    }

    .login:hover,
    .login:focus {
        background-position: 0 5px;
    }

</style>

@endsection @section('content')

<div class="container">


    <div class="columns">

        <div class="column is-half is-offset-one-quarter box">

            <h3 class="title is-3">Login</h3>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="field">
                    <p class="control">
                        <input class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" name="email" placeholder="Email Address">
                    </p>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="field">
                    <p class="control">
                        <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password" name="password" placeholder="password">
                    </p>
                    @if ($errors->has('password'))
                    <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>


                <div class="level">
                    <div class="level-left">
                        <div class="level-item">
                            <input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> {{ __('Remember Me') }}

                        </div>
                    </div>
                    <div class="level-right">
                        <div class="level-item">
                            <a href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                    </div>
                    <div class="level-right">
                        <div class="level-item">
                            <button class="button login" type="submit"> </button>
                        </div>
                    </div>
                </div>
        </div>

        </form>
    </div>
</div>

@endsection
