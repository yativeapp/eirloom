<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trustee extends Model
{

    protected $fillable = ['username', 'email', 'phonenumber', 'user_id'];
}
