<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{


    protected $fillable = ['video', 'name', 'user_id', 'email', 'thumb', 'birthday', 'avatar', 'cover'];


protected $casts = ['id' => 'string'];

    public function owner()
    {

        return $this->belongsTo(User::class, 'user_id');
    }
}
