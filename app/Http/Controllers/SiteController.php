<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plans;
use App\User;
use App\Mailing;
use App\Mail\Contact;

class SiteController extends Controller
{
    public function contact()
    {

        $data = request()->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'phonenumber' => 'required',
            'email' => 'required|email',
            'msg' => 'required',
        ]);

        $user = new User;

        $user->email = 'support@eirloom.com';

        \Mail::to($user)->send(new Contact($data));


        return redirect()->back()->with('updated', 'Thanks For Your Message');

    }


    public function mailing()
    {

        $data = request()->validate([
            'mailing_email' => 'required|email|unique:mailings'
        ]);


        Mailing::create($data);

        return redirect()->back()->with('updated', 'You Have Been Subscribed Successfully');

    }

    public function checkout()
    {
        if (!session()->has('prereg')) {
            return redirect('/');
        }

        $data = session('prereg');


        $off = 0;

        $plan_price = Plans::where('name', $data['plan'])->first();

        $plan_price = ($data['term'] == 'monthly') ? $plan_price->monthly : $plan_price->yearly;

        if (!empty($data['coupon'])) {


            try {
                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
                $coupon = \Stripe\Coupon::retrieve($data['coupon']);

                $off = $coupon['percent_off'];
            } catch (\Exception $e) {
            }

            $off_amount = $off * ($plan_price / 100);

            $plan_price = $plan_price - $off_amount;
        }


        return view('auth.checkout', compact('data', 'plan_price', 'off'));
    }


    public function getUsers()
    {

        return User::where('username', 'LIKE', '%' . request('name') . '%')->get()->map->username;
    }
}
