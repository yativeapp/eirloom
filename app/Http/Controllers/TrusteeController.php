<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trustee;
use App\Mail\Reciever;

class TrusteeController extends Controller
{


    public function store()
    {
        request()->validate([
            'trustee_email' => 'required|email',
            'trustee_name' => 'required',
            'trustee_number' => 'required'
        ]);

        $t = Trustee::firstOrCreate(['user_id' => auth()->id()]);

        $t->update([
            'username' => request('trustee_name'),
            'email' => request('trustee_email'),
            'phonenumber' => request('trustee_number')
        ]);

        return redirect()->back()->with('updated', 'Trustee Details Has Been Updated Successfully');

    }

    public function release()
    {

        $users = auth()->user()->profiles()->get()->filter(function ($p) {
            return is_string($p->email);
        })->each(function ($user) {
            \Mail::to($user)->send(new Reciever($user));
        });

        return redirect()->back()->with('updated', 'Mails were Sent Successfully');
    }
}
