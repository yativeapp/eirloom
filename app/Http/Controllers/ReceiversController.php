<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\File;

use Dilab\Network\SimpleRequest;
use Dilab\Network\SimpleResponse;
use Dilab\Resumable;
use App\Profiles;

class ReceiversController extends Controller
{


    public function index()
    {

        $profiles = auth()->user()->profiles()->get();

        return view('receivers', compact('profiles'));
    }

    public function getReciever($id)
    {
        $user = Profiles::with('owner.events')->where('id', $id)->first();

        $events = $user->owner->events;

        if (!$user) return redirect()->back();

        return view('user.reciever', compact('user', 'events'));
    }

    public function videoDownload(Profiles $reciever)
    {
        $uploadPath = storage_path('app/public/videos');

        return response()->download($uploadPath . '/' . $reciever->video);
    }

    /**
     * Handles resumeable uploads via resumable.js
     * 
     * @return Response
     */
    public function videoUpload()
    {
        $tmpPath = storage_path() . '/tmp';
        $uploadPath = storage_path('app/public/videos');
        if (!File::exists($tmpPath)) {
            File::makeDirectory($tmpPath, $mode = 0777, true, true);
        }

        if (!File::exists($uploadPath)) {
            File::makeDirectory($uploadPath, $mode = 0777, true, true);
        }

        $simpleRequest = new SimpleRequest();
        $simpleResponse = new SimpleResponse();

        $resumable = new Resumable($simpleRequest, $simpleResponse);
        $resumable->tempFolder = $tmpPath;
        $resumable->uploadFolder = $uploadPath;


        $result = $resumable->process();

        switch ($result) {
            case 200:
                return response([
                    'message' => 'OK',
                ], 200);
                break;
            case 201:

                $name = request('resumableFilename');
                $fileExt = pathinfo($name, PATHINFO_EXTENSION);
                $filename = md5($name) . '.' . $fileExt;

                File::move($uploadPath . '/' . $name, $uploadPath . '/' . $filename);

                Profiles::find(request('id'))->update(['video' => $filename]);

                return response([
                    'message' => 'OK',
                ], 200);
                break;
            case 204:
                return response([
                    'message' => 'Chunk not found',
                ], 204);
                break;
            default:
                return response([
                    'message' => 'An error occurred',
                ], 404);
        }
    }


    public function thumbUpload(Profiles $reciever)
    {

        $path = str_replace('public', '/storage', request('thumb')->store('public/images'));

        $reciever->update(['thumb' => $path]);
    }

    public function name(Profiles $reciever)
    {
        $reciever->update(['name' => request('name')]);
    }

    public function details(Profiles $reciever)
    {
        $reciever->update(['birthday' => request('birthday'), 'email' => request('email')]);
    }

    public function cover(Profiles $reciever)
    {
        $path = str_replace('public', '/storage', request('image')->store('public/images'));

        $reciever->update(['cover' => $path]);
    }

    public function avatar(Profiles $reciever)
    {

        $path = str_replace('public', '/storage', request('image')->store('public/images'));

        $reciever->update(['avatar' => $path]);

    }
}
