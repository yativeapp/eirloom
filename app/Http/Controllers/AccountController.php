<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AccountController extends Controller
{

    public function index()
    {
        $will = auth()->user()->will()->first();

        $user = auth()->user();


        $t = auth()->user()->trustee()->first();

        $s = auth()->user()->subscriptions()->first();

        $user_plan = $s->stripe_plan;


        //$user_plan = 'premuim monthly';



        return view('user.account', compact('will', 'user_plan', 't', 'user'));
    }


    public function basic()
    {

        $data = request()->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'phonenumber' => 'required',
            'email' => 'required|email',
        ]);

        auth()->user()->update($data);

        return redirect()->back()->with('updated', 'Your Info Has Been Updated Successfullay');

    }

    public function password()
    {
        request()->validate([
            'password' => 'required',
            'new_password' => 'required',
        ]);

        if (\Hash::check(request('password'), auth()->user()->password)) {
            auth()->user()->update(['password' => \Hash::make(request('new_password'))]);

            return redirect()->back()->with('updated', 'Your Info Has Been Updated Successfullay');
        }

        return redirect()->back()->with(['updated' => 'Wrong Password', 'fail' => true]);

    }

    public function plan()
    {

        $plan = str_replace('-', ' ', request('plan'));


        $s = auth()->user()->subscriptions()->first();

        auth()->user()->subscription($s->stripe_plan)->swap($plan);

        return redirect()->back()->with('updated', 'Your are Now Subscribed to ' . $plan . ' plan');
    }

    public function billing()
    {
        $data = request()->validate([
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ]);

        auth()->user()->update($data);

        return redirect()->back()->with('updated', 'Your Info Has Been Updated Successfullay');
    }

    public function card()
    {
        auth()->user()->updateCard(request('stripeToken'));

        return redirect()->back()->with('updated', 'Your Card Info Has Been Updated');

    }

    public function will()
    {

        // request()->validate([
        //     'will.*.email' => 'email',
        // ]);

        $will = collect(request('will'))->map(function ($w) {

            return json_encode($w);
        });

        auth()->user()->will()->update($will->toArray());

        return redirect()->back()->with('updated', 'will Info Has Been Updated');
    }


    public function deleteUser()
    {

        auth()->user()->delete();

        auth()->logout();

        return redirect('/');
    }
}
