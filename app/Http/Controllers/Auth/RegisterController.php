<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        return request()->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'phonenumber' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'plan' => 'required',
            'term' => 'required',
            'zip' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'agree' => 'accepted',
            'coupon' => 'present'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phonenumber' => $data['phonenumber'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'zip' => $data['zip'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {


        $user = $this->create($data = session('prereg'));

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $sub = $user->newSubscription($data['plan'], $data['plan'] . ' ' . $data['term']);

        if (!empty($data['coupon'])) {
            $sub = $sub->withCoupon($data['coupon']);
        }

        try {
            $sub->create(request('stripeToken'));

        } catch (\Exception $e) {
            return redirect()->back()->with(['payment_failed' => $e->getMessage(), 'fail' => true]);
        }

        $this->guard()->login($user);

        session()->forget('prereg');

        return redirect($this->redirectPath());
    }




    public function preRegister()
    {
        $data = $this->validator();

        session()->put('prereg', $data);

        return redirect()->route('checkout');
    }
}
