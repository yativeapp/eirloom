<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{


    public function index($nick = null)
    {

        $user = is_null($nick) ? auth()->user() : User::where('username' , str_replace('@' , '' ,$nick))->first();
        $disabled = is_null($nick) ? 0 : 1;

        if(!$user) abort(404);
    
        $events = $user->events()->get();

        return view('user.profile', compact('events' , 'disabled' , 'user'));
    }

    public function wisdom()
    {

        auth()->user()->update(['wisdom' => request('wisdom')]);

        return redirect()->back()->with('updated', 'Your Words Of Wisodm Has Been Changed');

    }

    public function uploadCover()
    {

        $path = str_replace('public', '/storage', request('image')->store('public/images'));

        auth()->user()->update(['cover' => $path]);

    }
    public function uploadAvatar()
    {

        $path = str_replace('public', '/storage', request('image')->store('public/images'));

        auth()->user()->update(['avatar' => $path]);

    }
}
