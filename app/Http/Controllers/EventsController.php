<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use Dilab\Network\SimpleRequest;
use Dilab\Network\SimpleResponse;
use Dilab\Resumable;
use App\Event;

class EventsController extends Controller
{


    public function validateData()
    {
        $validator = \Validator::make(request()->all(), [
            'title' => 'required',
            'place' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'date' => 'required',
            'people' => 'required',
            'msg' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        return response()->json([], 200);

    }

    public function destroy()
    {

        Event::where('id', request('id'))->delete();

        return redirect()->back()->with('updated', 'event Has Been Deleted');

    }

    public function store()
    {

        $tmpPath = storage_path() . '/tmp';
        $uploadPath = storage_path('app/public');
        if (!File::exists($tmpPath)) {
            File::makeDirectory($tmpPath, $mode = 0777, true, true);
        }

        if (!File::exists($uploadPath)) {
            File::makeDirectory($uploadPath, $mode = 0777, true, true);
        }

        $simpleRequest = new SimpleRequest();
        $simpleResponse = new SimpleResponse();

        $resumable = new Resumable($simpleRequest, $simpleResponse);
        $resumable->tempFolder = $tmpPath;
        $resumable->uploadFolder = $uploadPath;


        $result = $resumable->process();

        switch ($result) {
            case 200:
                return response([
                    'message' => 'OK',
                ], 200);
                break;
            case 201:

                $name = request('resumableFilename');
                $fileExt = pathinfo($name, PATHINFO_EXTENSION);
                $filename = md5($name) . '.' . $fileExt;

                $dist = $uploadPath . '/' . $filename;

                File::move($uploadPath . '/' . $name, $dist);

                return response([
                    'name' => $filename
                ], 200);
                break;
            case 204:
                return response([
                    'message' => 'Chunk not found',
                ], 204);
                break;
            default:
                return response([
                    'message' => 'An error occurred',
                ], 404);
        }


    }

    public function save()
    {

        $path = str_replace('public', '/storage', request('thumb')->store('public/images'));

        $data = request()->all();

        $data['thumb'] = $path;

        auth()->user()->events()->create($data);

        return response()->json([], 200);
    }
}
