<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'place',
        'attachment',
        'lat',
        'lng',
        'date',
        'people',
        'msg',
        'thumb'
    ];

    protected $dates = ['date'];


    public function getAttachmentAttribute($value)
    {
        return 'storage/' . $value;
    }

    public function getPeopleAttribute($value)
    {
        return preg_replace('/@([^\s\.]+)/', '<a class="tag is-primary" href="/profile/$1">$0</a>', $value);
    }

    public function isVideo()
    {
        if (preg_match('/(\.jpg|\.png|\.gif|\.jpeg)$/', $this->attachment)) {
            return false;
        } else {
            return true;
        }
    }

}
