<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Will extends Model
{

    protected $fillable = [
        'user_id',
        'facebook',
        'youtube',
        'googleplus',
        'linkedin',
        'instagram',
        'twitter',
        'snapchat',
        'pinterest',
        'blogger'
    ];

    protected $casts = [
        'facebook' => 'array',
        'youtube' => 'array',
        'googleplus' => 'array',
        'linkedin' => 'array',
        'instagram' => 'array',
        'twitter' => 'array',
        'snapchat' => 'array',
        'pinterest' => 'array',
        'blogger' => 'array'
    ];


}
