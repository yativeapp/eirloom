<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Billable, Notifiable, SoftDeletes;


    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {

            $user->username = str_slug($user->firstname) . '-' . rand(10, 1000);
        });

        static::created(function ($user) {

            $profiles = [];

            for ($i = 0; $i < 9; $i++) {
                $profiles[] = ['user_id' => $user->id, 'id' => (string)Str::uuid()];
            }

            $user->recievers()->insert($profiles);

            $user->will()->create([]);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $dates = ['deleted_at'];


    public function plan()
    {

        $subscription = $this->subscriptions()->first();

        return Plans::where('name', explode(' ', $subscription->stripe_plan)[0])->first();
    }

    public function profiles()
    {


        $count = $this->plan()->count;

        //$count = 3;

        return $this->hasMany(Profiles::class, 'user_id')->latest()->take($count);
    }

    public function recievers()
    {
        return $this->hasMany(Profiles::class, 'user_id');
    }

    public function will()
    {

        return $this->hasOne(Will::class, 'user_id');
    }

    public function trustee()
    {

        return $this->hasOne(Trustee::class, 'user_id');
    }


    public function events()
    {
        return $this->hasMany(Event::class, 'user_id')->orderBy('date', 'DESC');
    }

    public function wisdomAsList()
    {
        return explode(PHP_EOL, $this->wisdom);
    }
}
